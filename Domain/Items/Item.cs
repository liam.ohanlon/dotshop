using System;

namespace Domain.Items
{
    public class Item
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public double Price { get; set; }

        public Item(string itemName, string itemDescription, double price)
        {
            ItemName = itemName;
            ItemDescription = itemDescription;
            Price = price;
        }

        public Item(int itemId, string itemName, string itemDescription, double price)
        {
            ItemId = itemId;
            ItemName = itemName;
            ItemDescription = itemDescription;
            Price = price;
        }
    }
}