using System.Collections.Generic;

namespace Domain.Items
{
    public interface IItemsRepository
    {
        IEnumerable<Item> GetAll();
        Item Create(Item item);
    }
}