using System;
using Domain.Items;

namespace Domain.Orders
{
    public class LineItem
    {
        public Item Item { get; }
        public int Quantity { get; private set; }
        public double Total => Item.Price * Quantity;

        public LineItem(Item item, int quantity)
        {
            Item = item;
            IncreaseQuantity(quantity);
        }

        public void IncreaseQuantity(int quantity = 1)
        {
            if (quantity < 1)
            {
                throw new LineItemException();
            }

            this.Quantity += quantity;
        }

        public void DecreaseQuantity(int quantity = 1)
        {
            if (quantity < 1 || Quantity - quantity < 0)
            {
                throw new LineItemException();
            }

            this.Quantity -= quantity;
        }
    }

    [Serializable]
    public class LineItemException : Exception
    {
    }
}