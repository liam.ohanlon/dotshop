using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Items;

namespace Domain.Orders
{
    public class Order
    {
        public int OrderId { get; }
        public DateTime OrderDate { get; }
        public bool OrderPaid { get; }
        public IList<LineItem> LineItems { get; }
        public double Total => LineItems.Sum(l => l.Total);

        public Order()
        {
        }

        public Order(int orderId, DateTime orderDate, bool orderPaid)
        {
            OrderId = orderId;
            OrderDate = orderDate;
            OrderPaid = orderPaid;
            LineItems = new List<LineItem>();
        }

        public void AddLineItem(LineItem newLineItem)
        {
            if (newLineItem == null)
            {
                throw new ArgumentNullException(nameof(newLineItem));
            }

            var lineItem = LineItems.FirstOrDefault(li => li.Item.ItemId == newLineItem.Item.ItemId);

            if (lineItem == null)
            {
                LineItems.Add(newLineItem);
                return;
            }

            lineItem.IncreaseQuantity(newLineItem.Quantity);
        }
    }
}