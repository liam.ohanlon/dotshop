using System.Collections.Generic;

namespace Domain.Orders
{
    public interface IOrdersRepository
    {
        Order Get(int id);
        IEnumerable<Order> GetAll();
    }
}