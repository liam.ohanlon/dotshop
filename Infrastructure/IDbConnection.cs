using System.Data.SqlClient;

namespace Infrastructure
{
    public interface IDbConnection
    {
        SqlConnection GetDbConnection();
    }
}