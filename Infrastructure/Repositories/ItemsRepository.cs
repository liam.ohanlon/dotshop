using System.Collections.Generic;
using System.Linq;
using Dapper;
using Domain.Items;

namespace Infrastructure.Repositories
{
    public class ItemsRepository : IItemsRepository
    {
        private readonly IDbConnection _dbConnection;

        public ItemsRepository(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public IEnumerable<Item> GetAll()
        {
            using (var db = _dbConnection.GetDbConnection())
            {
                var sql = "SELECT * FROM Items";
                return db.Query<Item>(sql);
            }
        }

        public Item Create(Item item)
        {
            using (var db = _dbConnection.GetDbConnection())
            {
                var sql = @"INSERT INTO Items (ItemName, ItemDescription, Price) VALUES (@name, @desc, @price)
                  SELECT SCOPE_IDENTITY()";
                var getSql = "SELECT * FROM Items WHERE ItemId = @id";

                var id = db.Query<int>(sql, new {name = item.ItemName, desc = item.ItemDescription, price = item.Price})
                    .Single();
                return db.QuerySingle<Item>(getSql, new {id});
            }
        }
    }
}