using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Domain.Items;
using Domain.Orders;

namespace Infrastructure.Repositories
{
    public class OrderRepository : IOrdersRepository
    {
        private static string SQL_GET_ORDER_WITH_ITEMS =
            @"SELECT o.*, i.* FROM Orders o
                  INNER JOIN OrderItems oi on o.OrderId = oi.OrderId 
                  INNER JOIN Items i on oi.ItemId = i.ItemId ";

        private readonly IDbConnection _dbConnection;

        public OrderRepository(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public IEnumerable<Order> GetAll()
        {
            using (var db = _dbConnection.GetDbConnection())
            {
                return QueryForOrdersWithItems(db, SQL_GET_ORDER_WITH_ITEMS).Distinct().ToList();
            }
        }

        public Order Get(int id)
        {
            using (var db = _dbConnection.GetDbConnection())
            {
                var sql = SQL_GET_ORDER_WITH_ITEMS + "WHERE o.OrderId = @id";
                return QueryForOrdersWithItems(db, sql, new {id}).FirstOrDefault();
            }
        }

        private static IEnumerable<Order> QueryForOrdersWithItems(SqlConnection db, string sqlToExecute,
            object criteria = null)
        {
            var orders = new Dictionary<long, Order>();
            var rows = db.Query<Order, Item, Order>(sqlToExecute, (order, item) =>
            {
                if (!orders.TryGetValue(order.OrderId, out var orderEntry))
                {
                    orderEntry = order;
                    orders.Add(orderEntry.OrderId, orderEntry);
                }

                orderEntry.AddLineItem(new LineItem(item, 1));
                return orderEntry;
            }, splitOn: "ItemId", param: criteria).Distinct().ToList();
            return rows;
        }
    }
}