using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace Infrastructure
{
    public class DbConnection : IDbConnection
    {
        public DbConnection(IConfiguration configuration)
        {
            this._connectionString = configuration.GetConnectionString("Dotshop");
        }

        public SqlConnection GetDbConnection()
        {
            return new SqlConnection(_connectionString);
        }

        private readonly string _connectionString;
    }
}