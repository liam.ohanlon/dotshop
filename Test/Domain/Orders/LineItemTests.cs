using Domain.Items;
using Domain.Orders;
using Xunit;

namespace Test.Domain.Orders
{
    public class LineItemTests
    {
        [Fact]
        public void IncreaseQuantity_WithDefaultQuantityProvided_IncreasesQuantityByOne()
        {
            // Arrange
            var item = new Item(1, "Item", "Desc", 10.00);
            var lineItem = new LineItem(item, 1);
            
            // Act
            lineItem.IncreaseQuantity();

            // Assert
            Assert.Equal(2, lineItem.Quantity);
        }
        
        [Fact] 
        public void DecreaseQuantity_WithDefaultQuantityProvided_DecreasesQuantityByOne()
        {
            // Arrange
            var item = new Item(1, "Item", "Desc", 10.00);
            var lineItem = new LineItem(item, 1);
            
            // Act
            lineItem.DecreaseQuantity();

            // Assert
            Assert.Equal(0, lineItem.Quantity);
        }
        
        [Fact]
        public void DecreaseQuantity_WithQuantityZero_ThrowsException()
        {
            // Arrange
            var item = new Item(1, "Item", "Desc", 10.00);
            var lineItem = new LineItem(item, 1);
            
            // Act / Assert
            Assert.Throws<LineItemException>(() => lineItem.DecreaseQuantity(0));
        }
        
        [Fact]
        public void DecreaseQuantity_WithQuantityLessThanZero_ThrowsException()
        {
            // Arrange
            var item = new Item(1, "Item", "Desc", 10.00);
            var lineItem = new LineItem(item, 1);
            
            // Act / Assert
            Assert.Throws<LineItemException>(() => lineItem.DecreaseQuantity(-1));
        }
        
    }
}