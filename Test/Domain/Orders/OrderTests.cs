using System;
using Domain.Items;
using Domain.Orders;
using Xunit;

namespace Test.Domain.Orders
{
    public class OrderTests
    {
        [Fact]
        public void AddLineItem_NullLineItem_ThrowsException()
        {
            // Arrange
            var order = new Order(1, DateTime.Now, false);

            // Act / Assert
            Assert.Throws<ArgumentNullException>(() => order.AddLineItem(null));
        }

        [Fact]
        public void AddLineItem_ValidLineItem_UpdatesOrderTotalCorrectly()
        {
            // Arrange
            var order = new Order(1, DateTime.Now, false);
            var item = new Item(1, "Item", "Desc", 12.00);

            // Act
            order.AddLineItem(new LineItem(item, 3));

            // Assert
            Assert.Equal(36.00, order.Total);
        }

        [Fact]
        public void AddLineItem_LineItemExists_UpdatesOrderTotalCorrectly()
        {
            // Arrange
            var order = new Order(1, DateTime.Now, false);
            var item = new Item(1, "Item", "Desc", 12.00);
            order.AddLineItem(new LineItem(item, 1));
            
            // Act
            order.AddLineItem(new LineItem(item, 2));

            // Assert
            Assert.Equal(36.00, order.Total);
        }
    }
}