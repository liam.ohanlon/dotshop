using Api.Controllers;
using Api.Services;
using Domain.Orders;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using NSubstitute.ReturnsExtensions;
using Xunit;

namespace Test.Api.Controllers
{
    public class OrdersControllerTests
    {
        private readonly IOrdersService _ordersService = Substitute.For<IOrdersService>();
        private OrdersController _ordersController;

        public OrdersControllerTests()
        {
            _ordersController = new OrdersController(_ordersService);
        }

        [Fact]
        public void Get_InvalidIdReceived_ReturnsNotFound()
        {
            // Arrange
            _ordersService.Get(Arg.Any<int>()).ReturnsNull();

            // Act
            var actionResult = _ordersController.Get(1);

            // Assert
            Assert.IsType<NotFoundResult>(actionResult);
        }
        
        [Fact]
        public void Get_ValidIdReceived_ReturnsSuccess()
        {
            // Arrange
            _ordersService.Get(Arg.Any<int>()).Returns(new Order());

            // Act
            var actionResult = _ordersController.Get(1);

            // Assert
            Assert.IsType<OkObjectResult>(actionResult);
        }
    }
}