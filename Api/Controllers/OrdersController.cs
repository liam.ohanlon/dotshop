using System.Collections.Generic;
using Api.Services;
using Domain.Orders;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class OrdersController : ControllerBase
    {
        private readonly IOrdersService _ordersService;

        public OrdersController(IOrdersService ordersService)
        {
            _ordersService = ordersService;
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult Get(int id)
        {
            var order = _ordersService.Get(id);
            return order != null ? (IActionResult) Ok(order) : NotFound();
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(_ordersService.GetAll());
        }
    }
}