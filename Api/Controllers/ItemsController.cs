using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Dto;
using Api.Services;
using Domain.Items;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class ItemsController : ControllerBase
    {
        private readonly IItemsService _itemsService;

        public ItemsController(IItemsService itemsService)
        {
            _itemsService = itemsService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(_itemsService.GetAll());
        }

        [HttpPost]
        public IActionResult Create([FromBody] ItemDto itemDto)
        {
            return Ok(_itemsService.Create(itemDto));
        }
    }
}