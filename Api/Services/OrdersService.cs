using System.Collections.Generic;
using Domain.Orders;

namespace Api.Services
{
    public class OrdersService : IOrdersService
    {
        private readonly IOrdersRepository _ordersRepository;

        public OrdersService(IOrdersRepository ordersRepository)
        {
            _ordersRepository = ordersRepository;
        }

        public Order Get(int id)
        {
            return _ordersRepository.Get(id);
        }

        public IEnumerable<Order> GetAll()
        {
            return _ordersRepository.GetAll();
        }
    }
}