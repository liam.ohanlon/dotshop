using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Dto;
using Domain.Items;
using Microsoft.AspNetCore.Mvc;

namespace Api.Services
{
    public interface IItemsService
    {
        IEnumerable<Item> GetAll();
        Item Create(ItemDto itemDto);
    }
}