using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Dto;
using Domain.Items;

namespace Api.Services
{
    public class ItemsService : IItemsService
    {
        private readonly IItemsRepository _itemsRepository;

        public ItemsService(IItemsRepository itemsRepository)
        {
            _itemsRepository = itemsRepository;
        }

        public IEnumerable<Item> GetAll()
        {
            return _itemsRepository.GetAll();
        }

        public Item Create(ItemDto itemDto)
        {
            var item = new Item(itemDto.Name, itemDto.Description, itemDto.Price);
            return _itemsRepository.Create(item);
        }
    }
}