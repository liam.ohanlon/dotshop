using System.Collections.Generic;
using Domain.Orders;

namespace Api.Services
{
    public interface IOrdersService
    {
        Order Get(int id);
        IEnumerable<Order> GetAll();
    }
}