﻿using System.Runtime.CompilerServices;
using Api.Services;
using Domain.Items;
using Domain.Orders;
using Infrastructure;
using Infrastructure.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

[assembly: InternalsVisibleTo("Tests")]
namespace Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSwaggerGen(opt => opt.SwaggerDoc("v1", new Info() {Title = "DotShop", Version = "V1"}));

            services.AddSingleton<IDbConnection, DbConnection>();

            services.AddTransient<IItemsService, ItemsService>();
            services.AddTransient<IItemsRepository, ItemsRepository>();

            services.AddTransient<IOrdersService, OrdersService>();
            services.AddTransient<IOrdersRepository, OrderRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseSwagger();
            app.UseSwaggerUI(opt => opt.SwaggerEndpoint("/swagger/v1/swagger.json", "Dotshop"));
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}