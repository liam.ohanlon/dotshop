using System;
using System.ComponentModel.DataAnnotations;

namespace Api.Dto
{
    public class ItemDto
    {
        [Required] public string Name { get; set; }
        [Required] public string Description { get; set; }
        [Required] [Range(0, double.MaxValue)] public double Price { get; set; }
    }
}